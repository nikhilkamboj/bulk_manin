package com.example.batch_manin.configuration;

import com.arangodb.ArangoDB;
import com.arangodb.Protocol;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.AbstractArangoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableArangoRepositories(basePackages = {"com.example.batch_manin.repository"})
public class ArangoConfiguration extends AbstractArangoConfiguration{
    @Override
    protected ArangoDB.Builder arango() {
        return new ArangoDB.Builder().useProtocol(Protocol.HTTP_JSON);
    }

    @Override
    protected String database() {
        return "test";
    }
}
