package com.example.batch_manin.configuration;

import com.arangodb.springframework.core.convert.ArangoEntityReader;
import com.example.batch_manin.batch.ArangoDbItemReader;
import com.example.batch_manin.batch.ArangoDbProcessor;
import com.example.batch_manin.batch.ArangoDbWriter;
import com.example.batch_manin.model.InventoryEdge;
import com.example.batch_manin.model.Product;
import com.example.batch_manin.model.ProductDto;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;

@Configuration
@EnableBatchProcessing
@PropertySource("application.properties")
public class SpringBatchConfigurationFile {
    @Bean
    public Job createJob(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
                         ItemReader<Product> itemReader, ItemProcessor<Product, Product> itemProcessor,
                         ItemWriter<Product> itemWriter) {

        Step step = stepBuilderFactory.get("ETL-File-Load")
                .<Product, Product>chunk(100)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();

        Step step2 = stepBuilderFactory.get("DB-Processing")
                .<ProductDto, InventoryEdge>chunk(100)
                .reader(dbProductReader())
                .processor(dbProductProcessor())
                .writer(dbInventoryWriter())
                .build();


        return jobBuilderFactory.get("ETL-Load")
                .incrementer(new RunIdIncrementer())
                .start(step2)
                .build();
    }

    @Bean
    public FlatFileItemReader<Product> flatFileItemReader(@Value("${input}") Resource resource) {
        FlatFileItemReader<Product> ProductFlatFileItemReader = new FlatFileItemReader<>();
        ProductFlatFileItemReader.setResource(resource);
        ProductFlatFileItemReader.setName("CSV-READER");
        ProductFlatFileItemReader.setLinesToSkip(1);
        ProductFlatFileItemReader.setLineMapper(getLineMapper());



        return ProductFlatFileItemReader;
    }

    @Bean
    public LineMapper<Product> getLineMapper() {
        DefaultLineMapper<Product> defaultLineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames(new String[]{"id", "brand", "category", "subCategory", "subSubCategory",
         "wheelType", "displayName", "numericID", "length", "breadth", "height", "capacity", "size",
         "color", "hsnCode", "mrp", "standardConsumerDiscount", "standardTraderDiscount", "gstValue", "isImported",
         "deadWeight", "yearOfManfacturing", "quantity"});



        BeanWrapperFieldSetMapper<Product> beanWrapperFieldSetMapper = new BeanWrapperFieldSetMapper<>();
        beanWrapperFieldSetMapper.setTargetType(Product.class);

        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(beanWrapperFieldSetMapper);
        return defaultLineMapper;
    }

    @Bean
    public ItemReader<ProductDto> dbProductReader() {
        return new ArangoDbItemReader();
    }

    @Bean
    public ItemProcessor<ProductDto,InventoryEdge> dbProductProcessor() {
        return new ArangoDbProcessor();
    }

    @Bean
    public ItemWriter<InventoryEdge> dbInventoryWriter() {
        return new ArangoDbWriter();
    }
}
