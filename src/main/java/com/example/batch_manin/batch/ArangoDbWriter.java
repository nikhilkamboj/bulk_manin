package com.example.batch_manin.batch;

import com.example.batch_manin.model.InventoryEdge;
import com.example.batch_manin.repository.InventoryEdgeRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;



public class ArangoDbWriter implements ItemWriter<InventoryEdge> {

    @Autowired
    InventoryEdgeRepository inventoryEdgeRepository;

    @Override
    public void write(List<? extends InventoryEdge> list) throws Exception {
        inventoryEdgeRepository.saveAll(list);
    }
}
