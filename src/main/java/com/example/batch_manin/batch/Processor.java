package com.example.batch_manin.batch;

import com.example.batch_manin.model.Product;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Processor implements ItemProcessor<Product,Product> {
    private static final Map<String,String> Product_MAP = new HashMap<>();

    public Processor() {
        Product_MAP.put("001", "Technology");
        Product_MAP.put("002", "Operations");
        Product_MAP.put("003", "Accounts");
    }

    @Override
    public Product process(Product Product) throws Exception {
//        String ProductDeptId = Product.getDept();
//        String ProductDeptName = Product_MAP.get(ProductDeptId);
//        Product.setDept(ProductDeptName);
        return Product;
    }
}
