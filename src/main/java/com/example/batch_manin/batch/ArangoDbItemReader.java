package com.example.batch_manin.batch;

import com.example.batch_manin.model.Product;
import com.example.batch_manin.model.ProductDto;
import com.example.batch_manin.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class ArangoDbItemReader implements ItemReader<ProductDto>{

    @Autowired
    private ProductRepository productRepository;

    List<ProductDto> productList;

    Integer productIndex = 1;

    @Autowired
    private ModelMapper modelMapper;


    public ArangoDbItemReader() {

    }

    void initialize() {
        productList = new ArrayList<>();
        Iterable<Product> products = productRepository.findAll();
        for (Product p: products
             ) {
            productList.add(getProductDtoFrom(p));
        }
        productIndex = 0;
    }

    @Override
    public ProductDto read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        ProductDto nextProduct = null;


        if (productIndex < productRepository.count()) {
            Optional<Product> optional = productRepository.findById(productIndex);
            nextProduct = getProductDtoFrom(optional.get());
            productIndex++;
        }
        return nextProduct;
    }

    private ProductDto getProductDtoFrom(Product p) {
        return modelMapper.map(p, ProductDto.class);
    }
}
