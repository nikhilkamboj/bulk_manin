package com.example.batch_manin.batch;

import com.example.batch_manin.model.Product;
import com.example.batch_manin.repository.ProductRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Writer implements ItemWriter<Product>{
    @Autowired
    private ProductRepository ProductRepository;

    @Override
    public void write(List<? extends Product> Products) throws Exception {
        System.out.println("here are all the Products" +Products);
        ProductRepository.saveAll(Products);
    }
}
