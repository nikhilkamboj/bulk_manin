package com.example.batch_manin.batch;

import com.example.batch_manin.model.InventoryEdge;
import com.example.batch_manin.model.Outlet;
import com.example.batch_manin.model.Product;
import com.example.batch_manin.model.ProductDto;
import com.example.batch_manin.repository.OutletRepository;
import com.example.batch_manin.repository.ProductRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;


public class ArangoDbProcessor implements ItemProcessor<ProductDto, InventoryEdge> {

    @Autowired
    OutletRepository outletRepository;

    @Autowired
    ProductRepository productRepository;

    public ArangoDbProcessor() {
    }

    @Override
    public InventoryEdge process(ProductDto productDto) throws Exception {

        Optional<Product> productOptional = productRepository.findById(productDto.getId());
        Optional<Outlet> optionalOutlet = outletRepository.findById(1);
        Outlet outlet = optionalOutlet.get();

        Product product = productOptional.get();

        InventoryEdge inventoryEdge = new InventoryEdge(outlet,product);

        return inventoryEdge;
    }
}
