package com.example.batch_manin.model;

import com.arangodb.springframework.annotation.Edge;
import com.arangodb.springframework.annotation.From;
import com.arangodb.springframework.annotation.To;
import org.springframework.data.annotation.Id;


@Edge
public class InventoryEdge {
    @Id
    private String id;

    @From
    private Outlet outlet;

    @To
    private Product product;

    public InventoryEdge(final Outlet outlet, final Product product) {
        super();
        this.outlet = outlet;
        this.product = product;
    }
}
