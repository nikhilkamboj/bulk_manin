package com.example.batch_manin.model;


import com.arangodb.springframework.annotation.Document;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Document
public class User {
    @org.springframework.data.annotation.Id
    private Integer Id;
    private String name;
    private String dept;
    private Integer salary;

    public User(Integer id, String name, String dept, Integer salary) {
        Id = id;
        this.name = name;
        this.dept = dept;
        this.salary = salary;
    }

    public User() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "User{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", dept='" + dept + '\'' +
                ", salary=" + salary +
                '}';
    }
}

