package com.example.batch_manin.model;

import com.arangodb.springframework.annotation.Document;
import org.springframework.data.annotation.Id;

@Document
public class Product {
    @Id
    private Integer id;
    private String brand;
    private String category;
    private String subCategory;
    private String subSubCategory;
    private String wheelType;
    private String displayName;
    private String numericID;
    private Float length;
    private Float breadth;
    private Float height;
    private Float capacity;
    private String size;
    private String color;
    private String hsnCode;
    private Float mrp;
    private Float standardConsumerDiscount;
    private Float standardTraderDiscount;
    private Float gstValue;
    private String isImported;
    private Float deadWeight;
    private String yearOfManufacturing;
    private String quantity;

    public Product() {
    }

    public Product(Integer id, String brand, String category,
                   String subCategory, String subSubCategory,
                   String wheelType, String displayName,
                   String numericID, Float length,
                   Float breadth, Float height, Float capacity, String size,
                   String color, String hsnCode, Float mrp, Float standardConsumerDiscount,
                   Float standardTraderDiscount, Float gstValue, String isImported, Float deadWeight,
                   String yearOfManufacturing, String quantity) {
        this.id = id;
        this.brand = brand;
        this.category = category;
        this.subCategory = subCategory;
        this.subSubCategory = subSubCategory;
        this.wheelType = wheelType;
        this.displayName = displayName;
        this.numericID = numericID;
        this.length = length;
        this.breadth = breadth;
        this.height = height;
        this.capacity = capacity;
        this.size = size;
        this.color = color;
        this.hsnCode = hsnCode;
        this.mrp = mrp;
        this.standardConsumerDiscount = standardConsumerDiscount;
        this.standardTraderDiscount = standardTraderDiscount;
        this.gstValue = gstValue;
        this.isImported = isImported;
        this.deadWeight = deadWeight;
        this.yearOfManufacturing = yearOfManufacturing;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getSubSubCategory() {
        return subSubCategory;
    }

    public void setSubSubCategory(String subSubCategory) {
        this.subSubCategory = subSubCategory;
    }

    public String getWheelType() {
        return wheelType;
    }

    public void setWheelType(String wheelType) {
        this.wheelType = wheelType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getNumericID() {
        return numericID;
    }

    public void setNumericID(String numericID) {
        this.numericID = numericID;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getBreadth() {
        return breadth;
    }

    public void setBreadth(Float breadth) {
        this.breadth = breadth;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getCapacity() {
        return capacity;
    }

    public void setCapacity(Float capacity) {
        this.capacity = capacity;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public Float getMrp() {
        return mrp;
    }

    public void setMrp(Float mrp) {
        this.mrp = mrp;
    }

    public Float getStandardConsumerDiscount() {
        return standardConsumerDiscount;
    }

    public void setStandardConsumerDiscount(Float standardConsumerDiscount) {
        this.standardConsumerDiscount = standardConsumerDiscount;
    }

    public Float getStandardTraderDiscount() {
        return standardTraderDiscount;
    }

    public void setStandardTraderDiscount(Float standardTraderDiscount) {
        this.standardTraderDiscount = standardTraderDiscount;
    }

    public Float getGstValue() {
        return gstValue;
    }

    public void setGstValue(Float gstValue) {
        this.gstValue = gstValue;
    }

    public String getImported() {
        return isImported;
    }

    public void setImported(String imported) {
        isImported = imported;
    }

    public Float getDeadWeight() {
        return deadWeight;
    }

    public void setDeadWeight(Float deadWeight) {
        this.deadWeight = deadWeight;
    }

    public String getYearOfManufacturing() {
        return yearOfManufacturing;
    }

    public void setYearOfManufacturing(String yearOfManufacturing) {
        this.yearOfManufacturing = yearOfManufacturing;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
