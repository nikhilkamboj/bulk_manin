package com.example.batch_manin.model;

import com.arangodb.springframework.annotation.Document;
import com.arangodb.springframework.annotation.Rev;
import org.springframework.data.annotation.Id;

@Document
public class Outlet {
    @Id
    private String id;
    @Rev
    private String rev;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRev() {
        return rev;
    }

    public void setRev(String rev) {
        this.rev = rev;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
