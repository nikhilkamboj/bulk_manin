package com.example.batch_manin.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.example.batch_manin.model.InventoryEdge;

public interface InventoryEdgeRepository extends ArangoRepository<InventoryEdge,Integer> {
}
