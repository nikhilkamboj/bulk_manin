package com.example.batch_manin.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.example.batch_manin.model.User;

public interface UserRepository extends ArangoRepository<User,Integer> {
}
