package com.example.batch_manin.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.example.batch_manin.model.Product;

public interface ProductRepository extends ArangoRepository<Product, Integer> {
}
