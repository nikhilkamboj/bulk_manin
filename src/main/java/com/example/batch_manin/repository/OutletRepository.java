package com.example.batch_manin.repository;

import com.arangodb.springframework.repository.ArangoRepository;
import com.example.batch_manin.model.Outlet;

public interface OutletRepository extends ArangoRepository<Outlet,Integer> {
}
