package com.example.batch_manin.controller;

import com.example.batch_manin.model.Outlet;
import com.example.batch_manin.repository.OutletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/outlet")
public class LoadOutlet {

    @Autowired
    OutletRepository outletRepository;

    @GetMapping
    public void loadOutlet() {
        Outlet outlet = new Outlet();
        outlet.setId("1");
        outlet.setName("ABSCHD");
        outlet.setRev("1234");

        outletRepository.save(outlet);
    }
}
